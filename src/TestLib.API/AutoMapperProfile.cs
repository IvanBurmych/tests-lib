﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestLib.Business.Models;
using TestLib.Data.Entities;

namespace TestLib.API
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<UserRegisterModel, AppUser>();


            CreateMap<TestModel, Test>()
                .ReverseMap();
            CreateMap<QuestionModel, Question>()
                .ReverseMap();
            CreateMap<AnswerModel, Answer>()
                .ReverseMap();
            CreateMap<ResultModel, Result>()
                .ReverseMap();
        }
    }
}
