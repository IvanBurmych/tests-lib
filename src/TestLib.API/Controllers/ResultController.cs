﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using TestLib.Business.Interfaces;
using TestLib.Business.Models;
using TestLib.Data.Entities;

namespace TestLib.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ResultController : Controller
    {
        private readonly IResultService service;
        private readonly UserManager<AppUser> userManager;

        public ResultController(IResultService service, UserManager<AppUser> userManager)
        {
            this.service = service;
            this.userManager = userManager;
        }
        [Authorize(Roles = "Admin")]
        [HttpGet("")]
        public async Task<ActionResult<IEnumerable<ResultModel>>> GetAllResults()
        {
            return Ok(await service.GetAllAsync());
        }
        [Authorize]
        [HttpPost("add")]
        public async Task<IActionResult> AddTest(ResultAddModel model)
        {
            var user = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);

            ResultModel result = new ResultModel 
            { 
                UserId = user.Id, 
                TestName = model.TestName, 
                CorrectAnswersCount = model.CorrectAnswersCount, 
                WrongAnswersCount = model.WrongAnswersCount
            };

            await service.AddAsync(result);

            return Ok();
        }
        [Authorize]
        [HttpGet("my")]
        public async Task<ActionResult<IEnumerable<ResultModel>>> GetAllMyResults()
        {
            var user = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);

            return Ok(await service.GetAllMyAsync(user.Id));
        }
    }
}
