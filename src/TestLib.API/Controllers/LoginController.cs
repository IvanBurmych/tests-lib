﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using TestLib.API.Settings;
using TestLib.Business.Models;
using TestLib.Data.Entities;

namespace TestLib.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : Controller
    {
        private readonly UserManager<AppUser> userManager;
        private readonly JwtSettings jwtSettings;
        private readonly IMapper mapper;

        public LoginController(UserManager<AppUser> userManager, IOptionsSnapshot<JwtSettings> jwtSettings, IMapper mapper)
        {
            this.userManager = userManager;
            this.jwtSettings = jwtSettings.Value;
            this.mapper = mapper;
        }
        [HttpPost("SignIn")]
        public async Task<IActionResult> SignIn(UserLoginModel model)
        {
            var user = userManager.Users.SingleOrDefault(u => u.Email == model.Email);
            if (user is null)
            {
                return Unauthorized();
            }

            var userSigninResult = await userManager.CheckPasswordAsync(user, model.Password);
            if (userSigninResult)
            {
                var roles = await userManager.GetRolesAsync(user);
                return Ok( new { access_token = GenerateJwt(user, roles) });
            }
            return Unauthorized();
        }
        [HttpPost("SignUp")]
        public async Task<IActionResult> SignUp(UserRegisterModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Random random = new Random();
            int randomValue = random.Next(2, 7);

            var user = mapper.Map<UserRegisterModel, AppUser>(model);
            user.RandomNumber = randomValue;
            var userCreateResult = await userManager.CreateAsync(user, model.Password);

            if (userCreateResult.Succeeded)
            {
                return Created(string.Empty, string.Empty);
            }

            return Problem(userCreateResult.Errors.First().Description, null, 500);
        }
        private string GenerateJwt(AppUser user, IList<string> roles)
        {

            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.Id.ToString()),
                new Claim(JwtRegisteredClaimNames.Email, user.Email),
                new Claim(JwtRegisteredClaimNames.UniqueName, user.UserName)
            };

            var roleClaims = roles.Select(r => new Claim("role", r));
            claims.AddRange(roleClaims);

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtSettings.Secret));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expires = DateTime.Now.AddDays(Convert.ToDouble(jwtSettings.ExpirationInDays));

            var token = new JwtSecurityToken(
                issuer: jwtSettings.Issuer,
                audience: jwtSettings.Issuer,
                claims,
                expires: expires,
                signingCredentials: creds
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
