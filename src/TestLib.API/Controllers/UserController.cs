﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;
using TestLib.Business.Models;
using TestLib.Data.Entities;

namespace TestLib.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : Controller
    {
        private readonly UserManager<AppUser> userManager;
        private readonly IMapper mapper;

        public UserController(UserManager<AppUser> userManager, IMapper mapper)
        {
            this.userManager = userManager;
            this.mapper = mapper;
        }
        [Authorize(Roles = "Admin")]
        [HttpGet("{userName}/ToRole/{roleName}")]
        public async Task<IActionResult> AddUserToRole(string userName, string roleName)
        {
            var user = userManager.Users.SingleOrDefault(u => u.UserName == userName);

            var result = await userManager.AddToRoleAsync(user, roleName);

            if (result.Succeeded)
            {
                return Ok();
            }
            return Problem(result.Errors.First().Description, null, 500);
        }
        //[Authorize]
        //[HttpGet("results")]
        //public async Task<IActionResult> GetUserResults()
        //{
        //    var user = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
        //    return Ok(user.Results);
        //}
        //[Authorize]
        //[HttpPost("AddResults")]
        //public async Task<IActionResult> AddUserResult(ResultModel model)
        //{
        //    var result = mapper.Map<Result>(model);

        //    var user = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
        //    user.Results.Add(result);
        //    return Ok();
        //}
    }
}
