﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using TestLib.Business.Interfaces;
using TestLib.Business.Models;
using TestLib.Data.Entities;

namespace TestLib.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestController : Controller
    {
        private readonly ITestService service;
        private readonly UserManager<AppUser> userManager;

        public TestController(ITestService service, UserManager<AppUser> userManager)
        {
            this.service = service;
            this.userManager = userManager;
        }
        [Authorize(Roles = "Admin")]
        [HttpGet("")]
        public async Task<ActionResult<IEnumerable<TestModel>>> GetAllTests()
        {
            return Ok(await service.GetAllAsync());
        }
        [Authorize]
        [HttpGet("my")]
        public async Task<ActionResult<IEnumerable<TestModel>>> GetAllMyTests()
        {
            int randomNumber = userManager.FindByNameAsync(HttpContext.User.Identity.Name).Result.RandomNumber;
            return Ok(await service.GetAllByDivisorAsync(randomNumber));
        }
        [Authorize]
        [HttpGet("myById/{id}")]
        public async Task<ActionResult<IEnumerable<TestModel>>> GetMyTestById(int id)
        {
            var user = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            int randomNumber = user.RandomNumber;
            if(id % randomNumber == 0)
                return Ok(await service.GetByIdAsync(id));
            return (BadRequest());
        }
        [Authorize(Roles = "Admin")]
        [HttpPost("add")]
        public async Task<IActionResult> AddTest(TestModel model)
        {
            await service.AddAsync(model);

            return Ok();
        }
    }
}
