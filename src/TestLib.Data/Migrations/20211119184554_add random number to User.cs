﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TestLib.Data.Migrations
{
    public partial class addrandomnumbertoUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "RandomNumber",
                table: "AspNetUsers",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RandomNumber",
                table: "AspNetUsers");
        }
    }
}
