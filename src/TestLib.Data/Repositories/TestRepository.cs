﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestLib.Data.Entities;
using TestLib.Data.Interfaces;

namespace TestLib.Data.Repositories
{
    public class TestRepository : ITestRepository
    {
        private readonly DataContext context;
        public TestRepository(DataContext context)
        {
            this.context = context;
        }
        public async Task AddAsync(Test entity)
        {
            await context.Tests.AddAsync(entity);
            await context.SaveChangesAsync();
        }

        public async Task DeleteByIdAsync(int id)
        {
            Test test = await context.Tests.FindAsync(id);
            context.Tests.Remove(test);
            await context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Test>> GetAllAsync()
        {
            return await context.Tests
                .Include(x => x.Questions)
                    .ThenInclude(ques => ques.Answers)
                .ToListAsync();
        }

        public async Task<IEnumerable<Test>> GetAllByDivisorAsync(int divisor)
        {
            return await context.Tests
                .Where(t => t.Id % divisor == 0)
                .Include(x => x.Questions)
                    .ThenInclude(ques => ques.Answers)
                .ToListAsync();
        }

        public async Task<Test> GetByIdAsync(int id)
        {
            return await context.Tests
                .Include(x => x.Questions)
                    .ThenInclude(ques => ques.Answers)
                .FirstOrDefaultAsync(x => x.Id == id);
        }
    }
}
