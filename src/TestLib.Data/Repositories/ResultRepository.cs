﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestLib.Data.Entities;
using TestLib.Data.Interfaces;

namespace TestLib.Data.Repositories
{
    public class ResultRepository : IResultRepository
    {
        private readonly DataContext context;
        public ResultRepository(DataContext context)
        {
            this.context = context;
        }
        public async Task AddAsync(Result entity)
        {
            await context.Results.AddAsync(entity);
            context.SaveChanges();
        }

        public async Task<IEnumerable<Result>> GetAllAsync()
        {
            return await context.Results.ToListAsync();
        }

        public async Task<IEnumerable<Result>> GetAllMyAsync(Guid id)
        {
            return await context.Results.Where(x => x.UserId == id).ToListAsync();
        }
    }
}
