﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestLib.Data.Entities;

namespace TestLib.Data.Interfaces
{
    public interface IResultRepository
    {
        Task<IEnumerable<Result>> GetAllAsync();
        Task<IEnumerable<Result>> GetAllMyAsync(Guid id);
        Task AddAsync(Result entity);
    }
}
