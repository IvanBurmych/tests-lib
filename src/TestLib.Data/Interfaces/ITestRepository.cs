﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TestLib.Data.Entities;

namespace TestLib.Data.Interfaces
{
    public interface ITestRepository
    {
        Task<IEnumerable<Test>> GetAllAsync();
        Task<IEnumerable<Test>> GetAllByDivisorAsync(int divisor);
        Task<Test> GetByIdAsync(int id);
        Task AddAsync(Test entity);
        Task DeleteByIdAsync(int id);
    }
}
