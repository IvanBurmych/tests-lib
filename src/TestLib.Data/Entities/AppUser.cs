﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace TestLib.Data.Entities
{
    public class AppUser : IdentityUser<Guid>
    {
        public int RandomNumber { get; set; }
        public ICollection<Result> Results { get; set; }
    }
}
