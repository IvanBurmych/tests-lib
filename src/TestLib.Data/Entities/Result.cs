﻿using System;


namespace TestLib.Data.Entities
{
    public class Result
    {
        public int Id { get; set; }
        public Guid UserId { get; set; }
        public string TestName { get; set; }
        public int CorrectAnswersCount { get; set; }
        public int WrongAnswersCount { get; set; }
    }
}
