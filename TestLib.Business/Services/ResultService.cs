﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TestLib.Business.Interfaces;
using TestLib.Business.Models;
using TestLib.Data.Entities;
using TestLib.Data.Repositories;

namespace TestLib.Business.Services
{
    public class ResultService : IResultService
    {
        private readonly ResultRepository repository;
        private readonly IMapper mapper;
        public ResultService(ResultRepository repository, IMapper mapper)
        {
            this.repository = repository;
            this.mapper = mapper;
        }
        public async Task AddAsync(ResultModel model)
        {
            var result = mapper.Map<Result>(model);
            await repository.AddAsync(result);
        }

        public async Task<IEnumerable<ResultModel>> GetAllAsync()
        {
            var results = await repository.GetAllAsync();
            return mapper.Map<IEnumerable<Result>, IEnumerable<ResultModel>>(results);
        }

        public async Task<IEnumerable<ResultModel>> GetAllMyAsync(Guid id)
        {
            var results = await repository.GetAllMyAsync(id);
            return mapper.Map<IEnumerable<Result>, IEnumerable<ResultModel>>(results);
        }
    }
}
