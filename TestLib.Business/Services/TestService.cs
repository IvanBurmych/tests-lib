﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestLib.Business.Interfaces;
using TestLib.Business.Models;
using TestLib.Data.Entities;
using TestLib.Data.Repositories;

namespace TestLib.Business.Services
{
    public class TestService : ITestService
    {
        private readonly TestRepository repository;
        private readonly IMapper mapper;
        public TestService(TestRepository repository, IMapper mapper)
        {
            this.repository = repository;
            this.mapper = mapper;
        }
        public async Task AddAsync(TestModel model)
        {
            var test = mapper.Map<Test>(model);
            await repository.AddAsync(test);
        }

        public async Task DeleteByIdAsync(int id)
        {
            await repository.DeleteByIdAsync(id);
        }

        public async Task<IEnumerable<TestModel>> GetAllAsync()
        {
            var tests = await repository.GetAllAsync();
            return mapper.Map<IEnumerable<Test>, IEnumerable<TestModel>>(tests);
        }

        public async Task<IEnumerable<TestModel>> GetAllByDivisorAsync(int divisor)
        {
            var tests = await repository.GetAllByDivisorAsync(divisor);
            return mapper.Map<IEnumerable<Test>, IEnumerable<TestModel>>(tests);
        }

        public async Task<TestModel> GetByIdAsync(int id)
        {
            var test = await repository.GetByIdAsync(id);
            return mapper.Map<TestModel>(test);
        }
    }
}
