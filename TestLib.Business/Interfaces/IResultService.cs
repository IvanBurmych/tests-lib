﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TestLib.Business.Models;

namespace TestLib.Business.Interfaces
{
    public interface IResultService
    {
        Task<IEnumerable<ResultModel>> GetAllAsync();
        Task<IEnumerable<ResultModel>> GetAllMyAsync(Guid id);
        Task AddAsync(ResultModel model);
    }
}
