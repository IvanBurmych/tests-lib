﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TestLib.Business.Models;

namespace TestLib.Business.Interfaces
{
    public interface ITestService
    {
        Task<IEnumerable<TestModel>> GetAllAsync();
        Task<IEnumerable<TestModel>> GetAllByDivisorAsync(int divisor);
        Task<TestModel> GetByIdAsync(int id);
        Task AddAsync(TestModel model);
        Task DeleteByIdAsync(int id);
    }
}
