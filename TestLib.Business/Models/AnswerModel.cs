﻿
namespace TestLib.Business.Models
{
    public class AnswerModel
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public bool IsTrue { get; set; }
    }
}
