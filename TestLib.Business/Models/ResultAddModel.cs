﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestLib.Business.Models
{
    public class ResultAddModel
    {
        public string TestName { get; set; }
        public int CorrectAnswersCount { get; set; }
        public int WrongAnswersCount { get; set; }
    }
}
