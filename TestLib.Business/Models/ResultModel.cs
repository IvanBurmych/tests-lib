﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestLib.Business.Models
{
    public class ResultModel
    {
        public int Id { get; set; }
        public Guid UserId { get; set; }
        public string TestName { get; set; }
        public int CorrectAnswersCount { get; set; }
        public int WrongAnswersCount { get; set; }
    }
}
