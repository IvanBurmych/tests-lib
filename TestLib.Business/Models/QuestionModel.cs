﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestLib.Business.Models
{
    public class QuestionModel
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public ICollection<AnswerModel> Answers { get; set; }
    }
}
